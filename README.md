# Tokyo Olympic Data Analysis on Azure
<!-- ![TOKYO OLYMPIC DATA ANALYSIS](<Resources/Images/TOKYO OLYMPIC DATA ANALYSIS.jpeg>) -->

<img src="Resources/Images/TOKYO OLYMPIC DATA ANALYSIS.jpeg" alt="TOKYO OLYMPIC DATA ANALYSIS" width="350" height="350">

## Project Overview
The "Tokyo Olympic Data Analysis on Azure" project showcases a comprehensive end-to-end data engineering solution that leverages various Azure services to ingest, process, transform, and visualize Olympic Games data. This project demonstrates proficiency in multiple Azure services, data transformation, and visualization tools, making it an excellent addition to a data engineer's resume.

## Key Components and Technologies
- **Azure Data Factory**: For data ingestion from sources like GitHub.
- **Azure Data Lake Storage Gen2**: Primary storage solution for raw and transformed data.
- **Azure Databricks**: For data transformation tasks using PySpark.
- **Azure Synapse Analytics**: For advanced data analytics and creating structured schemas.
- **Power BI**: To create interactive dashboards based on the analyzed data.

## Dataset details
[2021 Olympics in Tokyo](https://www.kaggle.com/datasets/arjunprasadsarkhel/2021-olympics-in-tokyo)

## Workflow Steps
1. **Data Ingestion**: Data is ingested from a GitHub repository using Azure Data Factory and stored in Azure Data Lake Storage Gen2.
2. **Data Transformation**: Raw data undergoes transformation in Azure Databricks using PySpark notebooks, which includes cleaning, renaming columns, filtering, and aggregating data.
3. **Data Loading**: The transformed data is then loaded into Azure Synapse Analytics for in-depth analysis using SQL queries.
4. **Data Visualization**: Insights from the analyzed data are visualized using Power BI, creating interactive dashboards for stakeholders to explore trends and patterns.

## Project Benefits
- Demonstrates proficiency in a wide range of in-demand skills, including data ingestion, transformation, analysis, and visualization.
- Showcases the ability to design and implement scalable, efficient data pipelines capable of handling large datasets.
- Provides a practical, real-world example of leveraging cloud-based solutions to solve complex data challenges.

## Installation and Setup
To set up this project locally, follow these steps:

### Prerequisites
- Azure Subscription
- GitHub Account
- Power BI Desktop

### Installation
1. **Clone the repository**:
    ```bash
    git clone <CURRENT_REPO>
    cd Tokyo-Olympic-Data-Analysis
    ```

2. **Set up Azure Services**:
    - Create an Azure Data Factory instance.
    - Set up Azure Data Lake Storage Gen2.
    - Create an Azure Databricks workspace.
    - Set up Azure Synapse Analytics.
    - Install Power BI Desktop.

3. **Configure Data Factory**:
    - Create a pipeline in Azure Data Factory to ingest data from the GitHub repository.

4. **Transform Data in Databricks**:
    - Use PySpark notebooks in Azure Databricks to clean and transform the data.

5. **Load Data into Synapse Analytics**:
    - Load the transformed data into Azure Synapse Analytics for further analysis.

6. **Create Power BI Dashboards**:
    - Use Power BI Desktop to create interactive dashboards based on the data in Azure Synapse Analytics.

## Usage
This project can be used to analyze and visualize data from the Tokyo Olympic Games. The interactive dashboards created in Power BI allow stakeholders to explore data trends and patterns effortlessly.

## Credits 
Inspiration and methodology is credited to darshil parmar (Content Creator)
[Project Youtube Reference](https://www.youtube.com/watch?v=IaA9YNlg5hM&list=PLBJe2dFI4sgvQTNNkI3ETYJgNPR4CBpFd&index=7) 
p.s Great video for complete workflow 😊

## Contributing
Contributions are welcome! Please fork the repository and create a pull request with your changes. Ensure that your code follows the project's coding standards and includes appropriate tests.

## License
This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for more details.

## Contact
For any questions or suggestions, please contact:
- Venkatasai Kadamati - venkatasaikadamati@gmail.com

## Acknowledgments
- Azure Documentation
- GitHub Community
- Power BI Community
